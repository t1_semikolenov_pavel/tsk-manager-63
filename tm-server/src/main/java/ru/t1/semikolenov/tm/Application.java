package ru.t1.semikolenov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.semikolenov.tm.component.Bootstrap;
import ru.t1.semikolenov.tm.configuration.ContextConfiguration;

public final class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        context.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}