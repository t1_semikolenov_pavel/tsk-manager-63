package ru.t1.semikolenov.tm.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.semikolenov.tm.listener.EntityListener;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.semikolenov.tm")
public class ContextConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        return factory;
    };

    @Bean
    @NotNull
    public EntityListener logListener() {
        return new EntityListener();
    }

}
