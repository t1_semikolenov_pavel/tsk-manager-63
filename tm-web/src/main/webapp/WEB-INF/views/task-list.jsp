<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Task List</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">Project</th>
        <th width="200" nowrap="nowrap" align="left">Name</th>
        <th width="100%" align="left">Description</th>
        <th width="150" align="center" nowrap="nowrap">Status</th>
        <th width="100" align="center" nowrap="nowrap">Date Begin</th>
        <th width="100" align="center" nowrap="nowrap">Date End</th>
        <th width="100" align="center" nowrap="nowrap">Edit</th>
        <th width="100" align="center" nowrap="nowrap">Dalete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${projectRepository.findById(task.projectId).name}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateBegin}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateEnd}"/>
            </td>
            <td align="center">
                <a href="/task/edit/?id=${task.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/task/delete/?id=${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px">
    <button>Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
