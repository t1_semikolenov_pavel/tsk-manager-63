package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.model.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    {
        add("task 1");
        add("task 2");
        add("task 3");
    }

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final String name) {
        final Task task = new Task(name);
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}
