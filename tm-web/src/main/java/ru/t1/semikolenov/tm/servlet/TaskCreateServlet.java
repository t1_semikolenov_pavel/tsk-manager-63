package ru.t1.semikolenov.tm.servlet;

import ru.t1.semikolenov.tm.repository.TaskRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        TaskRepository.getInstance().add("new task");
        resp.sendRedirect("/tasks");
    }

}
