package ru.t1.semikolenov.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Nullable
    private String projectId = null;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
